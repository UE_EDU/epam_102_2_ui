# EPAM_102_2_Blueprint_Creating_User_Interfaces_UMG_Blueprints 

Developed with Unreal Engine 5

## Shooting Gallery 

In this task we suggest you create an own small game: ShootingGallery 

In common words ShootingGallery is a game where a player can shoot some targets and receive a score for any hit. Time and Shots should be limited. 

The main goal of this task is practice in using interfaces and UMG widgets and bindings. So please use them :) 

### Basic requirements: 

Game should start from a Menu. Two necessary buttons are:  

- Start Game: Start a new game with full time and ammo. Note: Timer should start after Start Game was clicked (not when Game Menu is loaded) 

- Exit: Close game window (like Esc) 

### During game: 

The player should be able to hit the targets 

Target should move a little during game (no matter how you implement it, but collision should move with mesh) 

There should be different types of targets (at least three). Different targets should give different amount of score 

If target is hit it should disappear and increase player’s score on correspondent value 

Each Shot should decrease amount of remaining shots 

Game should be limited by time and by shots. I mean if no time or no shots remain game should be stopped.

During the game the following values should be presented on screen: Remaining time, Remaining shots, Current score (presented using UMG) 

Both Remaining time and Remaining shots presented on screen should change color to some hint of red if their values become less than 5 (for example, if Remaining time is 6 or greater it can be written in white color but then it becomes less than 6 it should be written in red) 
 
Game should end with special Game over widget. The following buttons should be available: 

- Restart Game (should lead to Start Game menu) 

- Exit (as Exit button on Start Game menu) 

Also, on Game over widget user should be able to see Final score of the latest game 

### Suggestions: 

You can start from FirstPerson BP template game 

To make it clear for the user what type each target has, you can use materials of different colors (to save time you can use BasicAsset01, BasicAsset02 and BasicAsset03 materials) 

### Additional Tasks (if you want you can also implement the following) 

You can add an additional Screen for rewriting default values of Game Time and Ammo Amount 

For example:  

Create a button “Game settings” on Start screen and open specific widget on releasing this button. On this widget you can use a Slider to give the user an ability to set Game duration time. And TextBox to allow the user to input the start amount of the ammo  

Also, you can create another blueprint class “Ammo box” and place its instances on level. Instances of this class will increase the number of Remaining shots when Character overloads them (and disappears after such overloading) 

![FirstPersonBP.](/readme/BP_FPC.PNG "FirstPersonBP.")

![BP_AmmoBox.](/readme/BP_AmmoBox.PNG "BP_AmmoBox.")

![BP_Rifle.](/readme/BP_Rifle.PNG "BP_Rifle.")

![BP_Target.](/readme/BP_Target.PNG "BP_Target.")

![W_Bind.](/readme/W_Bind.PNG "W_Bind.")

![W_End.](/readme/W_End.PNG "W_End.")

![W_HUD.](/readme/W_HUD.PNG "W_HUD.")

![W_Settings.](/readme/W_Settings.PNG "W_Settings.")

![W_Start.](/readme/W_Start.PNG "W_Start.")

Demo: https://youtu.be/YP3PqiKdEzM